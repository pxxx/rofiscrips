#!/bin/bash

# Script name: rofi-translate.sh
# Description: look up words/terms
# Dependencies: rofi, browser(librewolf)

leourl="https://dict.leo.org/"
dudenurl="https://www.duden.de/suchen/dudenonline/"
# if you wont to look it up directly and not search search for it use:
# dudenurl="www.duden.de/rechtschreibung/"
browser="$(~/.config/sxhkd/variables browser1)"
if [[ "$browser" == *"librewolf"* ]] || [[ "$browser" == *"firefox"* ]]; then
  browser+=" --new-window"
fi

declare dictionary_options="englisch-deutsch\nspanisch-deutsch\nduden"

if command -v sdcv &>/dev/null; then
  dictionary_options="sdcv\n$dictionary_options"
fi

language=$(echo -e "$dictionary_options" | rofi -dmenu -i -p "translate")
[ -z "$language" ] && exit
word="$(rofi -dmenu -p "search term")"
[ -z "$word" ] && exit
word="$(sed 's/ /+/g' <<< "$word")"

if [ "$language" = "duden" ]; then
  if curl --output /dev/null --silent --head --fail "https://www.duden.de/rechtschreibung/$word"; then
    $browser "https://www.duden.de/rechtschreibung/$word"
  else
    if curl --output /dev/null --silent --head --fail "https://www.duden.de/rechtschreibung/$(echo ${word^})"; then
      $browser "https://www.duden.de/rechtschreibung/$(echo ${word^})"
    else
      $browser "$dudenurl$word"
    fi
  fi

elif [ "$language" = "sdcv" ]; then
  result="$(sdcv "$word")"

  if echo "$result" | grep -q "Your choice"; then
    choise="$(echo "$result" | grep "^[0-9]\+)" | rofi -dmenu -i -p "Your choice")"
    [ -z "$choise" ] && exit
    term="$(echo "$choise" | awk -F "-->" '{print $2}')"
    rofi -e "$(sdcv "$term")"
  else
    rofi -e "$result"
  fi

else
  $browser "$leourl$language/$word"
fi

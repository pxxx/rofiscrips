#!/bin/bash

# Script name: rofi-select-libewolf-profile.sh
# Description: select librewolf profile
# Dependencies: rofi, librewolf

parth=$HOME/.librewolf/profiles/

options=$(ls "$parth")
options+=("profile_manager")
choise=$(printf '%s\n' "${options[@]}" | rofi -dmenu -i -p "librewolf-profiles")

command="librewolf --profile $parth$choise"
echo "choise: $choise"
case "$choise" in
  "")                 exit                                  ;;
  "profile_manager")  echo profile manager && librewolf -p  ;;
  *)                  $command                              ;;
esac

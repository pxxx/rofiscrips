#!/bin/bash

# Description: This converts rofi-scripts to dmenu-scripts and inverse.

# XXX: in progress

# -i inverse (dmenu to rofi; default rofi to dmenu)

# supported rofi flags:
# -i (ignore case)
# -p (prompt (the displayed text))
# -l (number of lines to show)
# (-e (massage)) -> just replace with -p that's at least better then nothing # TODO: implement

# supported dmenu flags:
# -i (ignore case)
# -p (prompt (the displayed text))
# -l (number of lines to show)

declare outputFile
declare inputFile
if [ $@ == *"-r"* ]; then
  outputFile=$2
  inputFile=$2
# elif [ $@ == *"-t"* ]; then # target
else
  inputFile=$1
  outputFile=$1_
fi

if [ *"-R"* == $@ ]; then # reversed -> dmenu to rofi
  cat $inputFile -e "s/dmenu/rofi -dmenu/g" > $outputFile
else
fi

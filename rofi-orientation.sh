#!/bin/bash

# Script name: rofi-orientation.sh
# Description: change monitor orientation
# Dependencies: rofi, xrandr


monitor_options=$(xrandr --listmonitors | tail -n+2 | awk '{print $NF}')

[ "$(echo $monitor_options | wc -w)" -gt 1 ] && monitor_options=("all" "${monitor_options[@]}")

selected_monitor=$(printf '%s\n' "${monitor_options[@]}" | rofi -i -dmenu -p "select montor")
[ -z "$selected_monitor" ] && exit

orientations=(
  "normal"
  "inverted"
  "right"
  "left"
)

orientation=$(printf '%s\n' "${orientations[@]}" | rofi -dmenu -p "select orientation")
[ -z "$orientation" ] && exit

function set_orientation() {
  command="xrandr --output $1 --rotate $2"
  echo -e "command:\t$command"
  $command
}

if [ "$selected_monitor" == "all" ]; then
  for m in ${monitor_options[@]:1}; do
    set_orientation "$m" "$orientation"
  done
else
  set_orientation "$selected_monitor" "$orientation"
fi


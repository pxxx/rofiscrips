#!/bin/bash

# Script name: rofi-YoutubePlaylistSelector.sh
# Description: select youtube playlist and opoen
# it in a browser
# Dependencies: rofi, browser(librewolf)

browser="$(~/.config/sxhkd/variables browser1)"

# TODO: find a way to view/listen the playlists in the terminal

declare -a options=(
  "1.m_k_f"
  "2.ani_m_c"
  "3.m_k"
  "4.ani_m_f"
  "5.ani_m_x"
  "6.ani_m"
  "7.m_f_3"
)

choise="$(printf '%s\n' "${options[@]}" | rofi -dmenu -i -p "select playlist")"

declare cmd=""
case "$choise" in
  "")           exit;;
  "1.m_k_f")    cmd="$browser https://www.youtube.com/watch?v=TED5d608JH0&list=PLMwPvlPx5ClXDfmga-7rYktRL4xz2qnnN"  ;;
  "2.ani_m_c")  cmd="$browser https://www.youtube.com/watch?v=tG2HRaEsGCI&list=PLMwPvlPx5ClVu0ZR_3XwzQi479znJcvVr"  ;;
  "3.m_k")      cmd="$browser https://www.youtube.com/watch?v=vDdu_hr7Wu8&list=PLMwPvlPx5ClVTNLS48VAImf2vjQXDvV4V"  ;;
  "4.ani_m_f")  cmd="$browser https://www.youtube.com/watch?v=UjjTMNDZi-A&list=PLMwPvlPx5ClUlogSbbZIAoApwQHXeBOTW"  ;;
  "5.ani_m_x")  cmd="$browser https://www.youtube.com/watch?v=d5HT8HoiAHg&list=PLMwPvlPx5ClVgdD2UP91926_zwsIEGlgl"  ;;
  "6.ani_m")    cmd="$browser https://www.youtube.com/watch?v=CaksNlNniis&list=PLMwPvlPx5ClUbmlcZGzFY-ECPCLl1DDuP"  ;;
  "7.m_f_3")    cmd="$browser https://www.youtube.com/watch?v=3aldOx-jmZk&list=PLMwPvlPx5ClWoD0Uca_qRdGGGKnkBS6RL"  ;;
  "8.mn")       cmd="$browser https://www.youtube.com/playlist?list=PLMwPvlPx5ClV9JDhJCtDHa5fpKX_Ewt4V"            ;;
esac

echo "$cmd"
$cmd

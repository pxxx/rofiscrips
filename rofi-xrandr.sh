#!/bin/bash
get_str_untill_space() {
  echo "$1" | cut -d " " -f1
}

declare -A monitors=()
declare current_monitor=""
while read -r i; do # this will remove leading spaces on each line
  if [[ "$i" == *" connected "* ]]; then
    current_monitor="$(get_str_untill_space "$i")"
  elif [[ "$i" =~ ^[[:digit:]]+x[[:digit:]]+([[:blank:]]|$) ]]; then
    [[ -z "$current_monitor" ]] && continue # this case actually should never occur
    resolution="$(get_str_untill_space "$i")"
    if [[ -z "${monitors["$current_monitor"]}" ]]; then
      monitors["$current_monitor"]="$resolution"
    else
      monitors["$current_monitor"]="${monitors["$current_monitor"]} $resolution"
    fi
  else
    current_monitor=""
  fi
done < <(xrandr -q)

echo ------------------------------------
# print monitors with the respective resolution
for i in "${!monitors[@]}"; do
  echo "$i: ${monitors["$i"]}"
done
echo ------------------------------------

error() {
  echo "${@: 1: $#-1}" >&2
  s="${*: -1}"
  if ((s == 1)); then
    exit 1
  fi
  return 1
}

get_shared_reselutions_of_monitors() {
  declare -A mon=()
  monitor_names=("${!monitors[@]}")
  for m in "${!monitors[@]}"; do
    if (($# == 0)); then
      mon["$m"]="${monitors["$m"]}"
    elif [[ "$*" ==  *"$m"* ]]; then
      mon["$m"]="${monitors["$m"]}"
    fi
  done
  monitor_names=("${!mon[@]}")

  if ((${#mon[@]} < 2)); then
    echo ${mon["$monitor_names"]}
    return 0
  fi

  monitor_names=("${!mon[@]}")
  declare -a shared=("${mon["${monitor_names[0]}"]}")
  for ((i=1; i<${#mon[@]}; i++)); do
    current_mon="${monitor_names[i]}"
    current=(${mon["$current_mon"]})

    declare -a shared_=()
    for e in "${current[@]}"; do
      if [[ "${shared[*]}" == *$e*  ]]; then
        shared_+=("$e")
      fi
    done
    shared=("${shared_[@]}")
  done
  echo "${shared[@]}"
}

apply_to_each_screen() {
  screens=(${@: 2})
  options="${1}"
  cmd="xrandr $(printf " --output %s $options " "${screens[@]}" )"
  echo "cmd: $cmd"
  $cmd
}

apply_to_each_monitor_but() {
  declare -a monitors=($(for m in "${!monitors[@]}"; do
      if [[ "${*: 2}" != *"$m"* ]]; then
        printf '%s' " $m"
      fi
    done))
  ((${#monitors[@]} == 0)) && return 0
  apply_to_each_screen "${1}" ${monitor[@]}
}

only_leave_on() {
  apply_to_each_monitor_but "--off" "$@"
}

set_resolution() {
  apply_to_each_screen "--mode $1" "${@: 2}"
}

set_pos() {
  apply_to_each_screen "--pos $1" "${@: 2}"
}

turn_off() {
  apply_to_each_screen "--off" "${@}"
}

get_highest_resulution() {
  out=(${monitors["$1"]})
  echo "${out[0]}"
}

set_highest_resulution() {
  cmd="xrandr$(for m in "${@}"; do printf '%s' " --output $m --mode $(get_highest_resulution "$m")"; done)"
  echo "cmd: $cmd"
  $cmd
}

get_active_monitors() {
  xrandr --listactivemonitors | tail -n +2 | awk '{print $NF}'
}

get_inactive_monitors() {
  declare -a out=()
  active_monitors="$(get_active_monitors)"
  for m in ${!monitors[@]}; do
    if [[ "$active_monitors" != *"$m"* ]]; then
      out+=("$m")
    fi
  done
  echo "${out[*]}"
}

rofi_select_at_least_two_monitors_from_list(){
  monitor_names=($@)
  if ((${#monitor_names[@]} < 2)); then error "selected to less monitors" 1; fi
  # if ((${#monitor_names[@]} == 2)); then echo "${monitor_names[*]}"; return 0; fi
  declare curretn_selection=""
  declare -a selection=()

  while true; do
    options=(
      # $(if ((${#selection[@]} > 0)); then printf '%s' "done"; else printf '%s' "all"; fi)
      $(for e in "${monitor_names[@]}"; do
          if [[ "${selection[*]}" != *"$e"* ]]; then printf '%s' "$e "; fi
        done)
    )
    # ((${#options[@]} == 1)) && break
    if ((${#options[@]} == 0)); then
      break
    elif ((${#options[@]} == 1)) && ((${#monitor_names[@]} == 2)); then
      selection+=("${options[0]}")
      break
    fi
    curretn_selection=("$(printf '%b\n' "${options[@]}" | rofi -dmenu -i -p "add a monitor(all/a$(((${#selection[@]} >= 2)) && printf '%s' "| done/d"))")")
    case "$curretn_selection" in
      "") return ;;
      all|a) selection=${monitor_names[@]}; break ;;
      done|d) ((${#selection[@]} > 0)) && break ;;
      *) selection+=("$curretn_selection");;
    esac
  done
  echo "${selection[*]}"
}

rofi_select_at_least_two_monitors(){
  echo "$(rofi_select_at_least_two_monitors_from_list ${!monitors[@]})"
}

rofi_select_at_least_two_active_monitors(){
  echo "$(rofi_select_at_least_two_monitors_from_list $(get_active_monitors))"
}

rofi_select_at_least_two_inactive_monitors(){
  echo "$(rofi_select_at_least_two_monitors_from_list $(get_inactive_monitors))"
}

rofi_select_single_monitor_from_list(){
  monitor_names=($@)
  if ((${#monitor_names[@]} == 0)); then
    return 1
  elif ((${#monitor_names[@]} == 1)); then
    echo "${monitor_names[*]}"
    return 0
  fi
  options=(
    $(for e in "${monitor_names[@]}"; do
        if [[ "${selection[*]}" != *"$e"* ]]; then printf '%s' "$e "; fi
      done)
  )
  echo "$(printf '%b\n' "${options[@]}" | rofi -dmenu -i -p "select a monitor")"
}

rofi_select_single_monitor(){
  echo "$(rofi_select_single_monitor_from_list ${!monitors[@]})"
}

rofi_select_active_monitor(){
  echo "$(rofi_select_single_monitor_from_list $(get_active_monitors))"
}

rofi_select_inactive_monitor(){
  echo "$(rofi_select_single_monitor_from_list $(get_inactive_monitors))"
}

rofi_select_monitors_from_list(){
  monitor_names=($@)
  if ((${#monitor_names[@]} < 2)); then
    echo "${monitor_names[*]}"; return 1
  fi

  declare curretn_selection=""
  declare -a selection=()

  while true; do
    options=(
      # $(if ((${#selection[@]} > 0)); then printf '%s' "done"; else printf '%s' "all"; fi)
      $(for e in "${monitor_names[@]}"; do
          if [[ "${selection[*]}" != *"$e"* ]]; then printf '%s' "$e "; fi
        done)
    )
    # ((${#options[@]} == 1)) && break # use this if you add the all and done options to the dmenu
    ((${#options[@]} == 0)) && break
    curretn_selection=("$(printf '%b\n' "${options[@]}" | rofi -dmenu -i -p "add a monitor (all/a$(((${#selection[@]} > 0)) && printf '%s' "| done/d"))")")
    case "$curretn_selection" in
      "") return ;;
      all|a) selection=${monitor_names[@]}; break ;;
      done|d) break ;;
      *) selection+=("$curretn_selection");;
    esac
  done
  echo "${selection[*]}"
}

rofi_select_two_monitors(){
  monitor_names=("${!monitors[@]}")
  if ((${#monitor_names[@]} <= 2)); then
    echo "${monitor_names[*]}"; return
  fi

  declare choices=()
  for ((i=0; i<${#monitor_names[@]}-1; i++)); do
    for ((j=i+1; j<${#monitor_names[@]}; j++)); do
      choices+=("${monitor_names[i]} ${monitor_names[j]}")
    done
  done

  printf '%b\n' "${choices[@]}" | rofi -dmenu -i -p "select monitors"
}

rofi_select_monitors(){
  echo "$(rofi_select_monitors_from_list ${!monitors[@]})"
}

rofi_select_active_monitors(){
  echo "$(rofi_select_monitors_from_list $(get_active_monitors))"
}

rofi_select_inactive_monitors(){
  echo "$(rofi_select_monitors_from_list $(get_inactive_monitors))"
}

project() {
  selected_monitors=("${@}")
  if ((${#selected_monitors[@]} == 1)); then
    error "invalide number of arguments for function project; arguments: $*" 1
  fi

  if ((${#selected_monitors[@]} == 0)); then
    if (( ${#monitors[@]} == 2 )); then selected_monitors=(${!monitors[@]})
    else selected_monitors=($(rofi_select_monitors)); fi
  fi

  if ((${#selected_monitors[@]} < 2)); then return; fi

  # check if all selected_monitors exists
  for m in "${selected_monitors[@]}"; do
    if [[ -z "${monitors["$m"]}" ]]; then
      error "selected monitor($m) does not exist or was not recognized" 1
    fi
  done

  shared_resolutions=($(get_shared_reselutions_of_monitors ${selected_monitors[@]}))
  echo "shared_resolutions: ${shared_resolutions[*]}"

  if ((${#shared_resolutions[@]} == 0)); then
    error "no shared resolution found for the monitors: ${selected_monitors[*]}" 1
  fi
  resolution="${shared_resolutions[0]}"

  apply_to_each_screen "--mode $resolution --pos 0x0" ${selected_monitors[@]}
  only_leave_on ${selected_monitors[@]}
}

duplicate() {
  selected_monitors=("${@}")
  if ((${#selected_monitors[@]} > 2 || ${#selected_monitors[@]} == 1)); then
    error "invalide number of arguments for function duplicate; arguments: ${selected_monitors[*]}" 1
  fi
  if ((${#selected_monitors[@]} == 0)); then
    selected_monitors=($(rofi_select_two_monitors))
  fi
  echo "selection: ${selected_monitors[*]}"

  project "${selected_monitors[@]}"
}

expand_current () {

  if [[ -z "$(get_inactive_monitors)" ]]; then
    error "no inactive monitor available for expansion" 1
  fi

  # NOTE: this function only really works if you have a horizontal or vertical line of monitors
  declare -A allowed_directions=(["left"]="--left-of" ["right"]="--right-of" ["above"]="--above" ["below"]="--below")

  declare direction="$1"
  if [[ "${!allowed_directions[@]}" != *"$direction"* ]] || [[ -z "$direction" ]]; then
    direction="$(printf '%b\n' "${!allowed_directions[@]}"|rofi -dmenu -i -p "select direction for expansion")"
  fi

  selected_monitors=(${@: 2})
  if ((${#selected_monitors[@]} == 0)); then
    selected_monitors=($(rofi_select_inactive_monitors))
  fi

  if ((${#selected_monitors[@]} == 0 )); then
    error "no monitor selected/and selectable for expansion" 1
  fi

  get_monitor_left() {
    declare pos=-1 mon current_pos
    for e in $(xrandr --listactivemonitors|tail -n +2); do
      if [[ "$e" =~ [[:digit:]]+/[[:digit:]]+x[[:digit:]]+/[[:digit:]]+\+[[:digit:]]+\+[[:digit:]]+ ]]; then
        current_pos="$(echo "$e" | awk -F '+' '{printf $2}')"
      elif [[ ! "$e" =~ \+.* ]] && [[ ! "$e" =~ [[:digit:]]+: ]]; then
        if ((current_pos < pos)) || ((pos == -1)); then
          pos="$current_pos"
          mon="$e"
        fi
      fi
    done
    echo "$mon"
  }
  get_monitor_right() {
    declare pos mon current_pos
    for e in $(xrandr --listactivemonitors|tail -n +2); do
      if [[ "$e" =~ [[:digit:]]+/[[:digit:]]+x[[:digit:]]+/[[:digit:]]+\+[[:digit:]]+\+[[:digit:]]+ ]]; then
        current_pos="$(echo "$e" | awk -F '+' '{printf $2}')"
      elif [[ ! "$e" =~ \+.* ]] && [[ ! "$e" =~ [[:digit:]]+: ]]; then
        if ((current_pos >= pos)); then
          pos="$current_pos"
          mon="$e"
        fi
      fi
    done
    echo "$mon"
  }
  get_monitor_above() {
    declare pos=-1 mon current_pos
    for e in $(xrandr --listactivemonitors|tail -n +2); do
      if [[ "$e" =~ [[:digit:]]+/[[:digit:]]+x[[:digit:]]+/[[:digit:]]+\+[[:digit:]]+\+[[:digit:]]+ ]]; then
        current_pos="$(echo "$e" | awk -F '+' '{printf $3}')"
      elif [[ ! "$e" =~ \+.* ]] && [[ ! "$e" =~ [[:digit:]]+: ]]; then
        if ((current_pos < pos)) || ((pos == -1)); then
          pos="$current_pos"
          mon="$e"
        fi
      fi
    done
    echo "$mon"
  }
  get_monitor_below() {
    declare pos mon current_pos
    for e in $(xrandr --listactivemonitors|tail -n +2); do
      if [[ "$e" =~ [[:digit:]]+/[[:digit:]]+x[[:digit:]]+/[[:digit:]]+\+[[:digit:]]+\+[[:digit:]]+ ]]; then
        current_pos="$(echo "$e" | awk -F '+' '{printf $3}')"
      elif [[ ! "$e" =~ \+.* ]] && [[ ! "$e" =~ [[:digit:]]+: ]]; then
        if ((current_pos >= pos)); then
          pos="$current_pos"
          mon="$e"
        fi
      fi
    done
    echo "$mon"
  }

  for m in "${selected_monitors[@]}"; do
    cmd="xrandr --output $m ${allowed_directions["$direction"]} $(fun="get_monitor_$direction"; $fun) --mode $(get_highest_resulution "$m")"
    echo "cmd: $cmd"
    $cmd
  done
}

set_line() {
  declare orientation="$1"
  if [[ "$orientation" != "vertical" ]] && [[ "$orientation" != "horizontal" ]]; then
    orientation="$(echo -e "horizontal\nvertical" | rofi -dmenu -i -p "select orientation")"
  fi
  [[ "$orientation" != "vertical" ]] && [[ "$orientation" != "horizontal" ]] && return 0

  declare -a selected_monitors=(${@: 2})
  if [[ -z "${selected_monitors[*]}" ]]; then
    selected_monitors=($(rofi_select_at_least_two_monitors))
  fi
  (( ${#selected_monitors[@]} < 2 )) && return 0

  declare cmd="xrandr"
  for ((i=0; i<${#selected_monitors[@]}; i++)); do
    current_monitor="${selected_monitors[i]}"
    if ((i == 0)); then
      cmd+=" --output $current_monitor --pos 0x0 --mode $(get_highest_resulution "$current_monitor")"
    else
      cmd+=" --output $current_monitor --mode $(get_highest_resulution "$current_monitor") $(
        if [[ "$orientation" == "horizontal" ]]; then printf '%s\n' '--right-of'; else printf '%s\n' '--below'; fi
      ) ${selected_monitors[i-1]}"
    fi
  done
  echo "cmd: $cmd"
  $cmd
  only_leave_on ${selected_monitors[@]}
}

set_primary() {
  monitor="$(rofi_select_single_monitor)"
  [[ -z "$monitor" ]] && return 1
  cmd="xrandr --output $monitor --primary"
  echo "cmd: $cmd"
  $cmd
}

turn_off_monitors() {
  selected_monitors=($(rofi_select_active_monitors))
  if [[ -z "${selected_monitors[@]}" ]]; then return 0; fi
  turn_off ${selected_monitors[@]}
}

rofi_change_resolution () {
  selected_monitors=($(rofi_select_active_monitors))
  if [[ -z "${selected_monitors[@]}" ]]; then return 0; fi

  declare -a resolutions=($(get_shared_reselutions_of_monitors ${selected_monitors[@]}))
  declare selected_resolution="$(printf '%s\n' "${resolutions[@]}"|rofi -dmenu -i -p "select resolution")"
  if [[ -z "$selected_resolution" ]]; then rofi_change_resolution; return 0; fi
  set_resolution "$selected_resolution" ${selected_monitors[@]}
}

rofi_turn_on_monitors() {
  selected_monitors=($(rofi_select_inactive_monitors))
  if [[ -z "${selected_monitors[@]}" ]]; then return 0; fi
  set_highest_resulution ${selected_monitors[@]}
}

rofi_select_option() {
  declare -A rofi_options=(
    ["0. set line"]="set_line"
    ["1. duplicate"]="duplicate"
    ["2. project"]="project"
    ["3. expand current setup"]="expand_current"
    ["4. primary"]="primary"
    ["5. turn monitors on"]="rofi_turn_on_monitors"
    ["6. turn monitors off"]="turn_off_monitors"
    ["7. only leave on"]="only_leave_on"
    ["8. change resolution"]="rofi_change_resolution"
  )

  selection="$(printf '%s\n' "${!rofi_options[@]}" | sort | rofi -dmenu -i -p "select option")"
  [[ -z "$selection" ]] && exit 0
  fun=${rofi_options["$selection"]}
  echo "fun: $fun"
  $fun

  rofi_select_option
}

rofi_select_option

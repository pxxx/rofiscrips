#!/bin/bash

# Description: Should be a script to select the monitor resolution. It should also provide the option to select one resolution for all monitors.

# XXX: in progress

declare -a monitor_options=( $(xrandr --listmonitors | tail -n+2 | awk '{print $NF}') )

[ ${#monitor_options[@]} -gt 1 ] && monitor_options=("all" "${monitor_options[@]}")

selection="$(printf '%s\n' "${monitor_options[@]}" | rofi -dmenu -i -p "select monitor")"

if [ $selection == "all" ]; then
  declare -a all_resulutions=( $(xrandr | grep "x*\." | awk '{print $1}') )
  declare -a optial_resulutions

  printf '%s\n' ${all_resulutions[@]} | uniq
  # TODO: uniq doesn't work like I wont. For some ever reason it's not going to remove the duplicated resolutions
  for e in $(printf '%s\n' ${all_resulutions[@]} | uniq); do

    # echo "e: $e"
    # echo "e in res: $(printf '%s\n' ${all_resulutions[@]} | grep -c "$e")"
    # echo "num of monitors: $(xrandr --listmonitors | head -n 1 | awk '{print $NF}')"
    if [[ $(printf '%s\n' ${all_resulutions[@]} | grep -c "$e") == $(xrandr --listmonitors | head -n 1 | awk '{print $NF}') ]]; then
      optial_resulutions+="$e"
    fi
  done

  selected_resulution="$(printf '%s\n' ${optial_resulutions[@]} | rofi -dmenu -i -p "select resolution")"


else
  echo "option for single monitor not implemented by now"
fi

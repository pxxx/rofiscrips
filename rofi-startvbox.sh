#!/bin/bash

# Desctription: start a VirtulaBox

# vboxmanage startvm `ls "$HOME/VirtualBox VMs/" | rofi -dmenu -i -p "select vm"`


declare -a blackList=(
  "0_sharedFolders"
)

vm=`ls "$HOME/VirtualBox VMs/" | grep -v -x "$(printf '%s\|' ${blackList[@]})" | rofi -dmenu -i -p "select vm"`
vboxmanage startvm "$vm"
# run_in_firejail=`printf '%s\n' yes no  | rofi -dmenu -i -p "do you won't to run in firejail"`
# [ -z "$run_in_firejail" ] && exit
# if [ "$run_in_firejail" == "yes" ]; then
#   firejail firejail --profile=/etc/firejail/virtualbox.profile vboxmanage startvm "$vm"
# else
#   vboxmanage startvm "$vm"
# fi

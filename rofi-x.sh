#!/bin/bash

# Script name: rofi-x.sh
# Dependencies: rofi, screen-locker(light-locker)

# TODO: find beter name

declare lock_cmd="slock"

declare -A options

function add_cmd() {
  options+=(["$1"]="$2")
}

add_cmd '0.poweroff' "systemctl poweroff"
add_cmd '1.reboot' "systemctl reboot"
add_cmd '2.suspend' "systemctl suspend"
add_cmd '3.suspend and lock' "systemctl suspend && $lock_cmd"
add_cmd '5.hibernate' "systemctl hibernate"
add_cmd '7.hybrid-sleep' "systemctl hybrid-sleep"
add_cmd '8.lock-screen' "$lock_cmd"

action=$(printf '%b\n' "${!options[@]}"| sort | rofi -dmenu -i -p "select option")
[ -z "$action" ] && exit
if [ "$action" == "3.suspend and lock" ]; then # this command fore what ever reason doesn't work other wise
  systemctl suspend && $lock_cmd
else
  cmd="${options["$action"]}"
  echo "$cmd"
  $cmd
fi

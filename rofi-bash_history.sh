#!/bin/bash

# Script name: rofi-bash_history.sh
# Description: run command from bash history

cmd=$(tac ~/.bash_history | rofi -dmenu -p "history")
[ -z "$cmd" ] && exit
echo "$cmd"
$cmd

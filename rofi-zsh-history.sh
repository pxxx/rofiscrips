#!/bin/bash

# Script name: rofi-zsh-history.sh
# Description: run command from zsh history

in=$(tac ~/.zsh_history)

cmd=$(echo "$in" | rofi -dmenu -p "zsh history")
[ -z "$cmd" ] && exit
echo "$cmd"
$cmd

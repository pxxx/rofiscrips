#!/bin/bash

# Script name: rofi-archpackag.sh
# Description: seach for package in browser
# Dependencies: rofi, browser(librewolf)

browser="$(~/.config/sxhkd/variables browser1)"
pacman="https://archlinux.org/packages/?q="
aur="https://aur.archlinux.org/packages/?O=0&K="

repository=$(echo -e "pacman\naur" | rofi -dmenu -i -p "repository")
[ -z "$repository" ] && exit
searchterm=$(rofi -dmenu -i -p "search for package")
[ -z "$searchterm" ] && exit

if [ "$repository" = "pacman" ]; then
  c=$browser" "$pacman$searchterm
else
  c=$browser" "$aur$searchterm
fi
echo "$c"
$c

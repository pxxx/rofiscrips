#!/bin/bash

# Script name: rofi-select-browser-profile.sh
# Description: select browser profile
# Dependencies: rofi, browser(s)(librewolf, firefox)

declare librewolf_profiles_parth="$HOME/.librewolf/profiles/"
declare firefox_profiles_parth="$HOME/.mozilla/firefox/profiles/"

# declare librewolf_profile_taplate_path="$HOME/.librewolf/_pt/"

declare parth

declare -a browser_optrions=( "librewolf" "firefox" )
declare browser=$(echo "$(printf '%s\n' "${browser_optrions[@]}")" | rofi -dmenu  -i -p "chose browser")
case "$browser" in
  "librewolf")  parth=$librewolf_profiles_parth;;
  "firefox")    parth=$firefox_profiles_parth;;
  *)            exit                                      ;;
esac

declare options=$(ls $parth )
options+=("new trash profile" "profile_manager")
declare choise=$(printf '%s\n' "${options[@]}" | rofi -dmenu -i -p "$browser")

declare command
case "$choise" in
  "")                 exit                                        ;;
  "profile_manager")  command="$browser -p"                       ;;
  # "new trash profile") command="if [ -d $librewolf_profile_taplate_path ]; then cp $librewolf_profile_taplate_path $librewolf_profiles_parth/pt; $browser; fi"
  *)                  command="$browser --profile $parth$choise"  ;;
esac

echo $command
$command

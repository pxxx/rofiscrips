#!/bin/bash

# Description: select the primary monitor through xrandr

current_primary_monitor=$(xrandr | grep "primary" | awk '{print $1}')

options=( $(xrandr --listmonitors | tail -n+2 | awk '{print $NF}') )
options[0]="selected:${options[0]}" # xrandr lists the primary monitor first

selection=$( printf '%s\n' ${options[@]} | rofi -dmenu -i -p "select primery monitor")
[[ -z "$selection" || "$selection" == "${options[0]}" ]] && exit
xrandr --output $selection --primary
